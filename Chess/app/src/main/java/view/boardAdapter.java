package view;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import model.Board;
import model.Pieces;

/**
 * Adapter to set up the board on the chess app.
 */

public class boardAdapter extends BaseAdapter {

    private Context context;
    private Board[][] board;

    public boardAdapter(Context c, Board[][] board) {
        this.context = c;
        this.board = board;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getCount() {
        return 64;
    }


    //Sets the board with the background color as well as the piece
    public View getView(int pos, View changeView, ViewGroup parent) {

        ImageView iv;

        if (changeView == null) {
            iv = new ImageView(context);

            int size = parent.getWidth()/8;

            ViewGroup.LayoutParams lv = new GridView.LayoutParams(size, size);

            iv.setLayoutParams(lv);


            if(((pos/8)%2) == 0) {

                if(pos%2 == 0) {
                    iv.setBackgroundColor(Color.parseColor("#F1DFAF"));
                } else {
                    iv.setBackgroundColor(Color.parseColor("#A58B4A"));
                }

            } else {
                if(pos%2 == 0) {
                    iv.setBackgroundColor(Color.parseColor("#A58B4A"));
                } else {
                    iv.setBackgroundColor(Color.parseColor("#F1DFAF"));
                }
            }

            Pieces piece = board[pos/8][pos%8].getPiece();

            if(piece != null) {
                iv.setImageResource(context.getResources().getIdentifier(piece.toString(), "drawable", context.getPackageName()));
            }

        } else {
            iv = (ImageView) changeView;
        }

        return iv;

    }

}
