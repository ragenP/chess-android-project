package cs213.ragen.chess;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.io.File;
import java.util.Arrays;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {

    private RadioGroup radioGroup;
    private ListView listView;
    private RadioButton tempRadio;
    private ArrayAdapter mAdapter;
    private Button newGameButton, deleteButton, loadGameButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        radioGroup = (RadioGroup) findViewById(R.id.sortRadioGroup);
        listView = (ListView)findViewById(R.id.savedGamesListView);

        radioGroup.check(R.id.nameRadioButton);

        newGameButton = (Button) findViewById(R.id.gameStartButton);
        deleteButton = (Button) findViewById(R.id.deleteButton);
        loadGameButton = (Button) findViewById(R.id.loadGameButton);

        newGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newGameEvent();
            }
        });


        // Load list of games to show if there are any. Can't figure out how to get files since android
        // doesn't allow you to make your own folder and open files from there..
        //------------------------------
        //File file = new File(Context.getFilesDir(), "game1");
        //System.out.println("Length of files: " + files.length);
        /*
        System.out.println("After files part");
        System.out.println();
        if(radioGroup.getCheckedRadioButtonId() != -1) {
            System.out.println("Radio Button if thing");
            tempRadio = (RadioButton)findViewById(radioGroup.getCheckedRadioButtonId());

            if(tempRadio.getText().toString().compareTo("Name") == 0) {
                System.out.println("Selected index: Name");
                String[] sortedGames = sortByName(files);

                //Initial list of games
                final List<String> sortedNameList = new ArrayList<String>(Arrays.asList(sortedGames));
                mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, sortedNameList);
                listView.setAdapter(mAdapter);
            }else if(tempRadio.getText().toString().compareTo("Date") == 0) {
                System.out.println("Selected index: Date");
            }
        }
        */

    }


    public void newGameEvent() {
        Toast.makeText(this, "Creating new Game", Toast.LENGTH_SHORT).show();
        Bundle bundle = new Bundle();
        Intent intent = new Intent(this, chessboard.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }


    /**
     *
     * @param files
     * @return String[] games
     *
     * Gets a list of files for the saved games. Gives a sorted version based on name/date
     */
    public String[] sortByName(File[] files) {

        String[] games = new String[files.length];

        //Gets the names of all the files and stores it in games[]
        int j = 0;
        for(int i = 0; i < files.length; i++) {
            if(files[i].isFile()) {
                try {
                    Scanner sc = new Scanner(files[i]);
                    String name = sc.useDelimiter("`").next();
                    games[j] = name;
                    j++;
                    sc.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        //Sort list of names
        Arrays.sort(games);
        return games;
    }


}
