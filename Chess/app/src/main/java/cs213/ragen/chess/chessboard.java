package cs213.ragen.chess;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import controller.chessController;
import model.Board;
import model.playerColor;
import view.boardAdapter;

/**
 * Created by Ragen on 12/11/16.
 */

public class chessboard extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private chessController game;
    private boolean record;
    private static boolean timesRun = false;
    private String gameName;
    private TextView turnView;
    private GridView boardView;
    private View[] boardSelected;
    private int[] boardPositions;
    private boardAdapter adapter;
    private boolean drawPressed;
    private boolean drawPressedThisTurn;
    private boolean undoPressed;
    private boolean resignPressed;

    MediaPlayer backSound;

    private Button drawButton;
    private Button undoButton;
    private Button resignButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_board);

        backSound = MediaPlayer.create(this, R.raw.mozart);
        backSound.start();

//        if (!timesRun) {

            timesRun = true;
            //saveGame();
            this.game = new chessController();
            boardSelected = new View[2];
            boardPositions = new int[2];
            adapter = new boardAdapter(this, game.getBoard());
            turnView = (TextView)findViewById(R.id.turnView);

      //  }

        drawButton = (Button) findViewById(R.id.drawButton);

        drawButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                draw();
            }
        });

        undoButton = (Button) findViewById(R.id.undoButton);

        undoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                undo();
            }
        });

        resignButton = (Button) findViewById(R.id.resignButton);

        resignButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resign();
            }
        });


        final GridView chessBoardGridView = (GridView)findViewById(R.id.board);

        chessBoardGridView.setAdapter(adapter);

        chessBoardGridView.setOnItemClickListener(this);

        this.boardView = chessBoardGridView;

    }


    private void saveGame() {

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        record = true;

                        AlertDialog.Builder alert = new AlertDialog.Builder(chessboard.this);
                        alert.setTitle("Save Game");
                        alert.setMessage("Enter a name for this game");

                        final EditText input = new EditText(chessboard.this);
                        alert.setView(input);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                String value = input.getText().toString();
                                gameName = value;
                                finish();
                            }
                        });

                        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                record = false;
                                finish();
                            }
                        });

                        alert.show();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        record = false;
                        finish();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Would you like to record this game?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();


    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if (boardSelected[0] == null) {

            Board boardPressed = game.getBoard()[position/8][position%8];

            if (boardPressed.getPiece() == null) {
                //do nothing
                return;
            }

            if (boardPressed.getPiece().getPlayer().getColor() != game.getCurrentPlayer().getColor()) {
                //do nothing
                return;
            }

            boardSelected[0] = view;
            boardPositions[0] = position;

            view.setBackgroundColor(Color.parseColor("#FF7A7A"));

        } else {
            boardSelected[1] = view;
            boardPositions[1] = position;


            if (game.move(boardPositions[0], boardPositions[1])) {
                adapter.notifyDataSetChanged();
                boardView.setAdapter(adapter);
                switchTurn();

                if (game.whiteWin() || game.blackWin()) {

                    final String winner = game.whiteWin() == true ? "White" : "Black";

                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which){
                                case DialogInterface.BUTTON_POSITIVE:
                                    //Intent intent = getIntent();
                                    backSound.stop();
                                    endGame();
                                    timesRun = false;
                                    //startActivity(intent);
                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    //startActivity(new Intent(chessboard.this, MainActivity.class));
                                    backSound.stop();
                                    timesRun = false;
                                    endGame();
                                    break;
                            }
                        }
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage(winner + " wins!").setPositiveButton("Done", dialogClickListener).show();

                }
                else if (game.blackInCheck()) {

                    Toast.makeText(this, "Black Player in check", Toast.LENGTH_LONG).show();
                }
                else if (game.whiteInCheck()) {
                    Toast.makeText(this, "White Player in check", Toast.LENGTH_LONG).show();
                }


            } else {
                Toast toast = Toast.makeText(this, "Illegal Move", Toast.LENGTH_SHORT);
                toast.show();
            }

            //boardSelected[0].setBackgroundColor(updateColor(boardPositions[0]));
            boardSelected[0] = null;
            boardSelected[1] = null;
            undoPressed = false;

        }

    }

    private void switchTurn() {

        if(game.getCurrentPlayer().getColor().equals(playerColor.BLACK)){
            turnView.setText("Black's turn");
        } else if(game.getCurrentPlayer().getColor().equals(playerColor.WHITE)){
            turnView.setText("White's turn");
        }

    }


    private void draw() {
        if (!drawPressed) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Draw");
            builder.setMessage("Player requested a draw. Accept?");

            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    backSound.stop();
                    drawPressed = true;
                    drawPressedThisTurn = true;

                    endGame();
                }
            });

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    drawPressed = false;
                    drawPressedThisTurn = false;
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    private void undo(){
        if(!undoPressed){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Undo");
            builder.setMessage("Undo last turn?");

            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    undoPressed = true;
                }
            });

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    undoPressed = false;
                }
            });
            AlertDialog alert = builder.create();
            alert.show();

        }
    }

    private void resign(){
        if(!resignPressed){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Resign");
            builder.setMessage("Are you sure you want to resign?");

            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //END GAME AND ASK FOR SAVE

                    backSound.stop();

                    endGame();
                }
            });

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    resignPressed = false;
                }
            });
            AlertDialog alert = builder.create();
            alert.show();

        }
    }
    private void endGame(){

        saveGame();

        //finish();



    }


}
