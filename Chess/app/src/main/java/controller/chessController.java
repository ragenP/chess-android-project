package controller;


import java.util.LinkedList;

import model.Bishop;
import model.Board;
import model.King;
import model.Knight;
import model.Move;
import model.Pieces;
import model.Queen;
import model.Rook;
import model.pawn;
import model.player;
import model.playerColor;

public class chessController {

    private Board[][] board;
    private Pieces[] capturedW;
    private Pieces[] capturedB;
    private int numOfWCaptured;
    private int numOfBCaptured;
    private boolean blackInCheck;
    private boolean whiteInCheck;
    private boolean blackWins;
    private boolean whiteWins;
    private player black;
    private player white;
    private player turn;
    private LinkedList<Move> moves;

    public chessController () {

        board = new Board[8][8];
        black = new player(playerColor.BLACK);
        white = new player(playerColor.WHITE);
        black.setOpponent(white);
        white.setOpponent(black);
        blackInCheck = false;
        whiteInCheck = false;
        capturedW = new Pieces[16];
        capturedB = new Pieces[16];
        numOfWCaptured = 0;
        numOfBCaptured = 0;

        turn = white;

        //Tracking moves to undo
        moves = new LinkedList<Move>();

        //For initialize the board
        for (int i = 0; i < 8 ; i++) {
            for (int j = 0; j < 8; j++) {
                board[i][j] = new Board(i, j);
            }
        }

        placeItems(black);
        placeItems(white);

    }

    protected void placeItems(player player) {

        int row;

        if(player.getColor() == playerColor.WHITE) {
            row = 7;
        } else {
            row = 0;
        }

        Pieces[] pieces = new Pieces[]
                { new Rook(), new Knight(), new Bishop(), new Queen(), new King(), new Bishop(), new Knight(), new Rook()};

        player.setKing((King)pieces[4]);

        //Set special pieces (last rows)
        for (int i = 0; i < 8; i++) {
            pieces[i].setPos(board[row][i]);
            pieces[i].setBoard(board);
            board[row][i].setPiece(pieces[i]);
            board[row][i].getPiece().setPlayer(player);
            player.addPiece(pieces[i]);
            player.addUncapPiece(pieces[i]);
        }

        if(player.getColor() == playerColor.WHITE) {
            row = 6;
        } else {
            row = 1;
        }

        //set pawns
        for(int i = 0; i<8; i++) {
            Pieces p = new pawn();
            p.setPos(board[row][i]);
            p.setBoard(board);
            board[row][i].setPiece(p);
            board[row][i].getPiece().setPlayer(player);
            player.addPiece(p);
            player.addUncapPiece(p);
        }

    }

    public boolean move(int initPos, int finalPos) {

        blackInCheck = false;
        whiteInCheck = false;

        //check out of bounds
        if (initPos > 63 || initPos < 0 || finalPos > 63 || finalPos < 0) {
            return false;
        }

        //android position to (r,c) translation
        int c = initPos%8;
        int r = initPos/8;

        Board source = board[r][c];

        c = finalPos%8;
        r = finalPos/8;


        Board dest = board[r][c];

        Pieces sourcePiece = source.getPiece();

        Pieces destPiece = dest.getPiece();

        Pieces capture = null;

        //regular movement
        Move.moveType moveType = Move.moveType.NORM;

        //Clicked empty box
        if (sourcePiece == null) {
            return false;
        }

        //Clicked opponent
        if (sourcePiece.getPlayer().getColor() != getCurrentPlayer().getColor()) {
            return false;
        }


        if (!sourcePiece.canMove(dest)) {
            return false;
        }

        //Capture item
        if (destPiece != null) {

            destPiece.setPos(null);
            if (turn == white) {

                capturedB[numOfBCaptured++] = destPiece;
            }
            else {

                capturedW[numOfWCaptured++] = destPiece;
            }

            destPiece.getPlayer().removePiece(destPiece);
            capture = destPiece;

        }

        Move move = new Move(sourcePiece, capture, source, dest);

        //Castling
        if (castle(sourcePiece, dest)) {

            moveType = Move.moveType.CASTLE;
            if (sourcePiece.getPlayer().getColor() == playerColor.WHITE) {

                if (dest.getX() == 6) {

                    Pieces rook = board[7][7].getPiece();
                    rook.setPos(board[7][5]);
                    rook.addMoves();
                    board[7][5].setPiece(rook);
                    board[7][7].setPiece(null);
                }
                else if (dest.getX() == 2) {

                    Pieces rook = getBoard()[7][0].getPiece();
                    rook.setPos(getBoard()[7][3]);
                    rook.addMoves();
                    board[7][3].setPiece(rook);
                    board[7][0].setPiece(null);

                }
            }
            else {

                if (dest.getX() == 6) {

                    Pieces rook = board[0][7].getPiece();
                    rook.setPos(board[0][5]);
                    rook.addMoves();
                    board[0][5].setPiece(rook);
                    board[0][7].setPiece(null);

                }
                else if (dest.getX() == 2) {

                    Pieces rook = board[0][0].getPiece();
                    rook.setPos(board[0][3]);
                    rook.addMoves();
                    board[0][3].setPiece(rook);
                    board[0][0].setPiece(null);
                }
            }
        }

        source.setPiece(null);
        sourcePiece.setPos(dest);
        dest.setPiece(sourcePiece);
        sourcePiece.addMoves();



        if (((turn == white && dest.getY() == 0) || (turn == black && dest.getY() == 7)) && sourcePiece.getClass() == pawn.class) {

            Pieces promotedPiece = promotion("Q");
            promotedPiece.setPos(dest);
            promotedPiece.setNumOfMoves(sourcePiece.numOfMoves());
            promotedPiece.setPlayer(turn);
            promotedPiece.setBoard(board);
            sourcePiece = promotedPiece;
            dest.setPiece(sourcePiece);
        }


        Board oppK = turn.getOpponent().getKing().getPos();

        if (turn.getOpponent().getKing().isInCheck(oppK)) {

            if (turn.getColor() == playerColor.WHITE) {
                blackInCheck = true;
                if (black.getKing().checkmate(black.getKing().getPos())) {
                    whiteWins = true;
                }
            } else {
                whiteInCheck = true;
                if (white.getKing().checkmate(white.getKing().getPos())) {
                    blackWins = true;
                }
            }
        }

        move.changeType(moveType);
        move.setSrcPos(initPos);
        move.setFinalPos(finalPos);
        moves.add(move);
        turn = (turn == white) ? black : white;

        return true;
    }


    private boolean castle(Pieces sourcePiece, Board dest) {

        int xPos = Math.abs( dest.getX() - sourcePiece.getPos().getX());

        if (sourcePiece instanceof King && xPos == 2) return true;

        return false;
    }


    public Pieces promotion(String p) {

        if (p == null) {
            p = "Q";
        }

        switch(p.charAt(0)) {
            case 'Q':
                return new Queen();
            case 'B':
                return new Bishop();
            case 'N':
                return new Knight();
            case 'R':
                return new Rook();
            default:
                return new Queen();
        }
    }


    public Board[][] getBoard() { return board; }

    public Pieces[] getCapturedWhite() { return capturedW; }

    public Pieces[] getCapturedBlack() { return capturedB; }

    public player getCurrentPlayer() { return turn; }

    public int blackCaptureCount() { return numOfBCaptured; }

    public int whiteCaptureCount() { return numOfWCaptured; }

    public boolean blackInCheck() { return blackInCheck; }

    public boolean whiteInCheck() { return whiteInCheck; }

    public void nextTurn() {
        turn = turn == white ? black : white;
    }

    public boolean whiteWin() { return whiteWins; }

    public boolean blackWin() { return blackWins; }

    public LinkedList<Move> getMoves() { return moves; }


}
