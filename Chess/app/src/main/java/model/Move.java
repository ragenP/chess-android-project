package model;

public class Move {

    public static enum moveType {
        CASTLE, ENPASS, NORM
    }


    private Pieces piece;
    private Pieces captured;
    private Board initBoard;
    private Board finalBoard;
    private moveType type;
    private int initPos, finalPos;

    public Move (Pieces piece, Pieces captured, Board initBoard, Board finalBoard) {
        this.piece = piece;
        this.captured = captured;
        this.initBoard = initBoard;
        this.finalBoard = finalBoard;
    }

    public void changeType(moveType type) {
        this.type = type;
    }

    public moveType getType() {
        return type;
    }

    public Board getDestination() { return finalBoard; }

    public Board getSource() { return initBoard;}

    public void setCapture(Pieces piece) { this.captured = piece; }

    public Pieces getCapture() { return captured; }

    public Pieces getPiece() { return piece; }

    public void setSrcPos(int s) { this.initPos = initPos;}

    public int getSrcPos() { return initPos; }

    public void setFinalPos(int d) { this.finalPos = finalPos; }

    public int getFinalPos() { return finalPos; }

}
