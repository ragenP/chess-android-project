package model;

public class Queen extends Pieces {

	public String getSymbol() {
		if(getPlayer().getColor() == playerColor.BLACK) {
			return "bQ";
		} else {
			return "wQ";
		}

	}

	public String getFullID() {
		return "queen";
	}

    //Check if the path to where queen can go is clear or not
	public boolean isValidMove(Board finalPos) {

        //if moving in X only
        if(getPos().getX() == finalPos.getX()) {
            return this.isEmptyPath(finalPos);

        //Moving in y only
        } else if (getPos().getY() == finalPos.getY()) {
            return this.isEmptyPath(finalPos);

        //Diagonal path
        } else if (Math.abs(getPos().getX()-finalPos.getX()) == Math.abs(getPos().getY()-finalPos.getY())) {
            return this.isEmptyPath(finalPos);
        }

        return false;
	}

}
