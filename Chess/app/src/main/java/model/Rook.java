package model;

public class Rook extends Pieces {

	public String getSymbol() {
		if (getPlayer().getColor() == playerColor.BLACK) {
			return "bR";
		} else {
			return "wR";
		}
	}

	public String getFullID() {
		return "rook";
	}

	public boolean isValidMove(Board finalPos) {

			//Moving in x
		if (getPos().getX() == finalPos.getX()) {
			return this.isEmptyPath(finalPos);

			//Moving in y
		} else if (getPos().getY() == finalPos.getY()) {
			return this.isEmptyPath(finalPos);
		}

		return false;
	}

}
