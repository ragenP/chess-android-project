package model;

import java.util.ArrayList;

//implements player with pieces associated with each player.
public class player {

	private ArrayList<Pieces> pieces;

	private ArrayList<Pieces> uncapturedPieces;

	private player opponent;

	private King king;

	private playerColor color;

	public player(playerColor color) {
		this.color = color;
		pieces = new ArrayList<Pieces>(16);
		uncapturedPieces = new ArrayList<Pieces>(16);
	}

	public playerColor getColor() { return color; }

	public player getOpponent() { return opponent; }

	public void addPiece(Pieces piece) { pieces.add(piece); }

	public void addUncapPiece(Pieces piece) { uncapturedPieces.add(piece); }

	public void removePiece(Pieces piece) { uncapturedPieces.remove(piece); }

	public ArrayList<Pieces> getPieces() { return pieces; }

	public ArrayList<Pieces> getUncapturedPieces() { return uncapturedPieces; }

	public void setOpponent(player opponent) { this.opponent = opponent; }

	public void setKing(King king) { this.king = king; }

	public King getKing() { return king; }
	
	
}
