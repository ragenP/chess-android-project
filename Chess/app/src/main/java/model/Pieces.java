package model;

public abstract class Pieces {

	private Board[][] board;
	private Board pos;

	public abstract boolean isValidMove(Board dest);

	private player player;

	private int moves;

	public abstract String getSymbol();
	public abstract String getFullID();

    //Check if path is empty
    public boolean isEmptyPath(Board finalPos) {

        if (pos.getX() == finalPos.getX() && pos.getY() != finalPos.getY()) {

            int currPos = Math.min(pos.getY(), finalPos.getY()) + 1;
            int destPos = Math.max(pos.getY(), finalPos.getY()) - 1;

            //Moving in Y only
            while (currPos <= destPos) {
                if (board[currPos++][pos.getX()].getPiece() != null) {
                    return false;
                }
            }
            return true;

            //Moving in X only
        } else if (pos.getX() != finalPos.getX() && pos.getY() == finalPos.getY()) {

            int currPos = Math.min(pos.getX(), finalPos.getX()) + 1;
            int destPos = Math.max(pos.getX(), finalPos.getX()) - 1;

            while (currPos <= destPos) {
                if (board[pos.getY()][currPos++].getPiece() != null) {
                    return false;
                }
            }
            return true;

            //Moving diagonally
        } else if (Math.abs(pos.getX() - finalPos.getX()) == Math.abs(pos.getY() - finalPos.getY())) {

            int xDisp = (pos.getX() < finalPos.getX()) ? 1 : -1;
            int yDisp = (pos.getY() < finalPos.getY()) ? 1 : -1;
            int currX  = xDisp + pos.getX();
            int currY = yDisp + pos.getY();

            while ((xDisp == 1 && currX < finalPos.getX()) || (xDisp == -1 && currX > finalPos.getX())){
                if (board[currY][currX].getPiece() != null){

                    return false;
                }
                currX += xDisp;
                currY += yDisp;
            }
            return true;
        }

        return false;
    }


    //Check if it is a valid move
	public boolean canMove(Board dest) {

		if (pos.equals(dest)) {
            return false;
        }

		if (dest.getPiece() != null) {
			if (player.getColor() == dest.getPiece().getPlayer().getColor()) {
				return false;
			}
		}

		if (!isValidMove(dest))  {
            return false;
        }

		Board kingPos = null;

		if (this instanceof King) {
			kingPos = dest;
		}
		else {
			kingPos = getPlayer().getKing().getPos();
		}

		if (getPlayer().getKing().isInCheck(kingPos)) {
            return false;
        }

		return true;
	}

    public void setBoard(Board[][] board) { this.board = board; }
    public Board getPos() { return pos; }
    public Board[][] getBoard() { return board; }

    public void setPos(Board location) { this.pos = location; }

    public player getPlayer() { return player; }
    public void setPlayer(player player) { this.player = player; }


    public void setNumOfMoves(int moves) { this.moves = moves; }
    public int numOfMoves() { return moves; }
    public void addMoves() { moves++; }
    public void decreaseMoves() { moves--; }


    public String colorString() {

        return getPlayer().getColor() == playerColor.WHITE ? "white" : "black";
    }

    //Returns name of the piece. i.e: "white_rook" which we can use to get resource from drawable
    public String toString() {
        return colorString()+"_"+getFullID();
    }


	
}
