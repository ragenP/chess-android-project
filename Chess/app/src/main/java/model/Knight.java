package model;

public class Knight extends Pieces {


	public String getSymbol() {
		if(getPlayer().getColor() == playerColor.BLACK) {
			return "bN";
		} else {
			return "wN";
		}
	}

	public String getFullID() {
		return "knight";
	}


	//Check if Knight move is valid
	public boolean isValidMove(Board finalPos) {
		//A move is valid when the x is 1 and y is 2 or when x is 2 and y is 1.
		if (Math.abs(finalPos.getX()-getPos().getX()) == 2 && Math.abs(finalPos.getY() - getPos().getY()) == 1) {
			return true;
		} else if (Math.abs(finalPos.getX()-getPos().getX()) == 1 && Math.abs(finalPos.getY() - getPos().getY()) == 2){
			return true;
		} else {
			return false;
		}
	}




	/*
	int color;	//if color == 1, it is white. else, it is black.
	public Knight(int color) {
		this.color = color;
	}
	
	public String boardID() {
		if(this.isWhite()) {
			return "wN";
		} else {
			return "bN";
		}
	}
	
	//If color == white, return true. Else, return false.
	public boolean isWhite() {
		if (this.color == 1) {
			return true;
		}
		return false;
	}
	
	
	public boolean isValidMove(Pieces[][] p, int[] currPos, int[] finalPos) {
		
		//Either moving up 2 and to the right/left one or moving down 2 and to the right/left one
		if(Math.abs(currPos[0]-finalPos[0]) == 2) {
			if(Math.abs(currPos[1]-finalPos[1]) == 1) {	//return true if the move is up/down 2 and right/left one.
				return true;
			}else {
				return false;
			}
			
			// Moving left/right 2 spaces and up/down 1
		} else if (Math.abs(currPos[1]-finalPos[1]) == 2) {
			if(Math.abs(currPos[0]-finalPos[0]) == 1) {
				return true;
			}else {
				return false;
			}
		}
		
		return false;
		
	}
	*/
	
}
