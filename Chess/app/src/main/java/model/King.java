package model;

public class King extends Pieces {


    public String getSymbol() {
        if(getPlayer().getColor() == playerColor.BLACK) {
            return "bK";
        } else {
            return "wK";
        }

    }

	public String getFullID() {
		return "king";
	}

	public boolean isValidMove (Board finalPos) {
		int xPos = Math.abs( finalPos.getX() - getPos().getX());
		int yPos= Math.abs( finalPos.getY() - getPos().getY());

		//Move is within one square
		if(xPos <= 1 && yPos <= 1) {
			return true;
		}

		//Check for castling
		if (getPlayer().getColor() == playerColor.BLACK) {
			if (getPos().getY() == 0 && getPos().getX() == 4 && numOfMoves() == 0) {
				if (finalPos.getX() == 6 && finalPos.getY() == 0) {
					Pieces rook = getBoard()[finalPos.getX() + 1][finalPos.getY()].getPiece();

                    //if the piece is not a rook or it has already moved
					if(rook.getClass() != Rook.class && rook.numOfMoves() != 0) {
						return false;
					} else {
                        return isEmptyPath(finalPos);
                    }
                    //castling other side
				} else if (finalPos.getX() == 2 && finalPos.getY() == 0) {
                    Pieces rook = getBoard()[finalPos.getX() - 2][finalPos.getY()].getPiece();

                    //if the piece is not a rook or it has already moved
                    if(rook.getClass() != Rook.class && rook.numOfMoves() != 0) {
                        return false;
                    } else {
                        return isEmptyPath(finalPos);
                    }
                }
			}

            return false;

            //White piece
		} else {

            if (getPos().getY() == 7 && getPos().getX() == 4 && numOfMoves() == 0) {
                if (finalPos.getX() == 6 && finalPos.getY() == 7) {
                    Pieces rook = getBoard()[finalPos.getX() + 1][finalPos.getY()].getPiece();

                    //if the piece is not a rook or it has already moved
                    if (rook.getClass() != Rook.class && rook.numOfMoves() != 0) {
                        return false;
                    } else {
                        return isEmptyPath(finalPos);
                    }
                    //castling other side
                } else if (finalPos.getX() == 2 && finalPos.getY() == 7) {
                    Pieces rook = getBoard()[finalPos.getX() - 2][finalPos.getY()].getPiece();

                    //if the piece is not a rook or it has already moved
                    if (rook.getClass() != Rook.class && rook.numOfMoves() != 0) {
                        return false;
                    } else {
                        return isEmptyPath(finalPos);
                    }
                }
            }

            return false;
        }

	}


    //Checks if king is in check.
    public boolean isInCheck (Board kingPos) {

        for(Pieces p: getPlayer().getOpponent().getPieces()) {

            if(p.getPos() == null) {
                return false;
            } else if(p.isValidMove(kingPos) && p.getPos() != null) {
                return true;
            }
        }

        return false;
    }



    public boolean checkmate(Board kingPos) {

        Board forward = new Board(kingPos.getY() + 1, kingPos.getX());
        Board backward = new Board(kingPos.getY() - 1, kingPos.getX());
        Board rightUp = new Board(kingPos.getY() + 1, kingPos.getX() + 1);
        Board leftUp = new Board(kingPos.getY() + 1, kingPos.getX() - 1);
        Board rightDown = new Board(kingPos.getY() - 1, kingPos.getX() + 1);
        Board leftDown = new Board(kingPos.getY() - 1, kingPos.getX() - 1);


        //Check if king is in check if we go forward/back left right etc.
        if (forward.getX() >= 0 && forward.getX() <= 7 && forward.getY() >= 0 && forward.getY() <= 7) {
            if (!isInCheck(forward) && getBoard()[forward.getY()][forward.getX()].getPiece() == null) {
                return false;
            }
        }

        if (backward.getX() >= 0 && backward.getY() <= 7 && backward.getY() >= 0 && backward.getY() <= 7) {
            if (!isInCheck(backward) && getBoard()[backward.getY()][backward.getX()].getPiece() == null) {
                return false;
            }
        }

        if (rightUp.getX() >= 0 && rightUp.getY() <= 7 && rightUp.getY() >= 0 && rightUp.getY() <= 7) {
            if (!isInCheck(rightUp) && getBoard()[rightUp.getY()][rightUp.getX()].getPiece() == null) {
                return false;
            }
        }

        if (leftUp.getX() >= 0 && leftUp.getY() <= 7 && leftUp.getY() >= 0 && leftUp.getY() <= 7) {
            if (!isInCheck(leftUp) && getBoard()[leftUp.getY()][leftUp.getX()].getPiece() == null) {
                return false;
            }
        }

        if (rightDown.getX() >= 0 && rightDown.getY() <= 7 && rightDown.getY() >= 0 && rightDown.getY() <= 7) {
            if (!isInCheck(rightDown) && getBoard()[rightDown.getY()][rightDown.getX()].getPiece() == null) {
                return false;
            }
        }

        if (leftDown.getX() >= 0 && leftDown.getY() <= 7 && leftDown.getY() >= 0 && leftDown.getY() <= 7) {
            if (!isInCheck(leftDown) && getBoard()[leftDown.getY()][leftDown.getX()].getPiece() == null) {
                return false;
            }
        }

        return true;

    }



	/*
	public boolean isValidMove(Pieces[][] p, int[] currPos, int[] finalPos) {

		//CASTLING for white piece
		if(this.isWhite()) {
			if(currPos[0] == 7 && currPos[1] == 4) {
				if (finalPos[0] == 7 && finalPos[1] == 7) {		//CASTLING MOVE
					if(p[currPos[0]][currPos[1]+1] == null && p[currPos[0]][currPos[1]+2] == null) {
						return true;
					} else {
						return false;
					}
				}
			}
		} else {	//CASTLING for black piece
			if(currPos[0] == 0 && currPos[1] == 4) {
				if (finalPos[0] == 0 && finalPos[1] == 7) {		//CASTLING MOVE
					if(p[currPos[0]][currPos[1]+1] == null && p[currPos[0]][currPos[1]+2] == null) {
						return true;
					} else {
						return false;
					}
				}
			}
		}

		//moving and capturing your own piece!!
		if(p[finalPos[0]][finalPos[1]] != null && p[finalPos[0]][finalPos[1]].isWhite() == p[currPos[0]][currPos[1]].isWhite()) {
			return false;
		}

		//Moving regularly anywhere
		//Only checks if you can move up down left right etc. 1 move anywhere.
		if(Math.abs((currPos[0] - finalPos[0])) == 1) {		//if the move is within 1 row.
			if(Math.abs(currPos[1] - finalPos[1]) == 1) {	//move is also within 1 column Moving DIAGONALLY
				return true;
			} else if (currPos[1] - finalPos[1] == 0) {		//moving vertically ONLY
				return true;
			}
		} else if (currPos[0] - finalPos[0] == 0) {		// moving horizontally ONLY
			if(Math.abs(currPos[1] - finalPos[1]) == 1) {	//move is also within 1 column
				return true;
			}
		} else if (Math.abs((currPos[0] - finalPos[0])) > 1 || Math.abs((currPos[1] - finalPos[1])) > 1) {
			return false;
		}

		return false;
	}
	*/
	
	
}
