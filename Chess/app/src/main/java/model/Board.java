package model;

public class Board {

	private int x;
    private int y;
    private Pieces p;

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    //initialize the specific board piece with row and column number
    public Board (int row, int column) {
        this.x = column;
        this.y = row;
    }

    public void setPiece(Pieces piece) {
        this.p = piece;
    }

    public Pieces getPiece() {
        return p;
    }


    //Check if the piece is equal to the piece on the board.
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Board)) {
            return false;
        }

        if (this.getX() == ((Board) o).getX() && this.getY() == ((Board) o).getY()) {
            return true;
        }

        return false;
    }

}
