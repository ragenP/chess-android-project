package model;

public class Bishop extends Pieces {

	public String getSymbol() {
		if(getPlayer().getColor() == playerColor.BLACK) {
			return "bB";
		} else {
			return "wB";
		}
	}

	public String getFullID() {
		return "bishop";
	}

	//Check if bishop move is valid
	public boolean isValidMove(Board finalPos) {
		//A move is valid when the x and y displacements are the same
		if (Math.abs(finalPos.getX()-getPos().getX()) != Math.abs(finalPos.getY() - getPos().getY())) {
			return false;
		} else {
			return this.isEmptyPath(finalPos);
		}
	}

}
